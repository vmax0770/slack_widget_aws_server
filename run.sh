#!/bin/bash

apt update
apt install -y python3-pip

git clone https://bitbucket.org/vmax0770/slack_widget_aws_server.git

cd slack_widget_aws_server
mkdir uploads

pip3 install -r requirements.txt
nohup python3 ./app.py
