from flask import Flask, request, send_file, jsonify
import uuid, os

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = 'uploads/'

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        f = request.files['file']
        upload_id = str(uuid.uuid4())
        filename = os.path.join(app.config['UPLOAD_FOLDER'], upload_id) + '.wav'
        f.save(filename)
        return jsonify({
            'upload_id': upload_id
        })
    return '''
          <!doctype html>
          <title>Upload new File</title>
          <h1>Upload new File</h1>
          <form method=post enctype=multipart/form-data>
            <p><input type=file name=file>
               <input type=submit value=Upload>
          </form>
          '''

@app.route('/get/<upload_id>')
def get(upload_id):
    return send_file(os.path.join(app.config['UPLOAD_FOLDER'], upload_id) + '.wav', 'audio/wav')


@app.route('/gets/<upload_id>')
def gets(upload_id):
    fn = '/get/' + upload_id
    return '''
              <!doctype html>
              <title>File</title>
              <h1>File <small>{upload_id}</small></h1>
              <audio controls="controls">
              <source src="{fn} " type="audio/wav" />
              </audio>
              '''.format(**locals())

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)

