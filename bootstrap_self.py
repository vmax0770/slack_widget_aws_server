import boto3, os
import time

AMI_ID = "ami-d8bdebb8" # Ubuntu Server 16.04 LTS (HVM), SSD Volume Type

ACCESS_KEY='AKIAIAC75OH675C3DDDA'
SECRET_KEY='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'

os.environ['AWS_DEFAULT_REGION'] = 'us-west-1'

ec2 = boto3.client('ec2', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)

SECURITY_GROUP = "group-allow-80-http-x"
SECURITY_GROUP_DESCRIPTION = "Allow in/out connections on ports 80"

script_filename = "run.sh"

try:
    script_file = open(script_filename)
    script = script_file.read()
    script_file.close()
except Exception as e:
    print("Error %s while reading script: %s" % (e, script_filename))

print("Importing public key...")
ec2.import_key_pair(
    KeyName='slack_flask_server_key_x',
    PublicKeyMaterial=open('key.pem.pub').read()
)

print("Creating a security group to allow connections on port 80...")
security_group = ec2.create_security_group(GroupName=SECURITY_GROUP, Description=SECURITY_GROUP_DESCRIPTION)
security_group = security_group['GroupId']

ec2.authorize_security_group_ingress(
    GroupId=security_group,
    IpPermissions=[
        {
            'IpProtocol': 'tcp',
            'FromPort': 22,
            'ToPort': 22,
            'IpRanges': [
                {'CidrIp': '0.0.0.0/0'}
            ]
        },
        {
            'IpProtocol': 'tcp',
            'FromPort': 80,
            'ToPort': 80,
            'IpRanges': [
                {'CidrIp': '0.0.0.0/0'}
            ]
        },
    ]
)

print("Allocating Elastic IP for the instance...")
addr = ec2.allocate_address()['PublicIp']

print("Firing up the instance...")
resp = ec2.run_instances(
    MinCount=1,
    MaxCount=1,
    ImageId=AMI_ID,
    InstanceType='t2.micro',
    UserData=script,
    KeyName='slack_flask_server_key_x',
    SecurityGroupIds=[security_group],
)

instance_id = resp['Instances'][0]['InstanceId']

print("Waiting for instance to come alive to associate an Elastic IP with it...")
instances = ec2.describe_instances(InstanceIds=[instance_id])
while instances['Reservations'][0]['Instances'][0]['State']['Name'] != 'running':
    time.sleep(3)
    instances = ec2.describe_instances(InstanceIds=[instance_id])

print("Associating address...")
ec2.associate_address(
    InstanceId=instance_id,
    PublicIp=addr
)

print("Instance ID: " + instance_id)
print("Associated ID: " + addr)
